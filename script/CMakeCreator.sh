echo "cmake_minimum_required(VERSION 3.6.0)
find_package(TDAQ)
set(TDAQ_DB_PROJECT HTT)
tdaq_project(HTT ${HTT_RELEASE_VERSION} USES tdaq 8.3.1)
" > CMakeLists.txt;
